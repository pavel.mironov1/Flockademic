jest.mock('../../src/services/periodical', () => ({
  getPeriodicals: jest.fn().mockReturnValue(Promise.resolve([
    {
      creator: { identifier: 'arbitrary_account_id' },
      identifier: 'arbitrary_slug',
      name: 'Arbitrary name',
    },
  ])),
}));

import {
  JournalIndexPage,
} from '../../src/components/journalIndexPage/component';
import { Spinner } from '../../src/components/spinner/component';
import { mockRouterContext } from '../mockRouterContext';

import { mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import * as React from 'react';

function initialisePage() {
  return mount(
    <JournalIndexPage match={{ url: 'https://arbitrary_url' }}/>,
    mockRouterContext,
  );
}

it('should display a spinner while the journals are being fetched', () => {
  const page = initialisePage();

  expect(page.find(Spinner)).toExist();
});

it('should display an error message when the journals could not be fetched', (done) => {
  const mockedPeriodicalService = require.requireMock('../../src/services/periodical');
  mockedPeriodicalService.getPeriodicals.mockReturnValueOnce(Promise.reject('Service error'));

  const page = initialisePage();

  setImmediate(checkAssertions);
  function checkAssertions() {
    page.update();
    expect(page.find('.alert')).toExist();
    done();
  }
});

it('should pass the list of journals to the journal index', (done) => {
  const mockedPeriodicalService = require.requireMock('../../src/services/periodical');
  mockedPeriodicalService.getPeriodicals.mockReturnValueOnce(Promise.resolve([
    {
      creator: { identifier: 'some_account_id' },
      identifier: 'some_slug',
      name: 'Some name',
    },
  ]));
  const page = initialisePage();

  setImmediate(checkAssertions);
  function checkAssertions() {
    page.update();
    expect(page.find('JournalIndex').prop('periodicals'))
      .toEqual([
        {
          creator: { identifier: 'some_account_id' },
          identifier: 'some_slug',
          name: 'Some name',
        },
      ]);
    done();
  }
});
